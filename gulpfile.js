"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var mqpacker = require("css-mqpacker");
var minify = require("gulp-csso");
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin");
var svgstore = require("gulp-svgstore");
var svgmin = require("gulp-svgmin");
var server = require("browser-sync").create();
var run = require("run-sequence");

gulp.task("style", function() {
  gulp.src("src/sass/style.scss")
    .pipe(plumber())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({browsers: [
        "last 1 version",
        "last 2 Chrome versions",
        "last 2 Firefox versions",
        "last 2 Opera versions",
        "last 2 Edge versions"]}),
      mqpacker({
        sort: true
      })
    ]))
    .pipe(gulp.dest("build/css"))
    .pipe(minify())
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest("build/css"))
    .pipe(server.stream());
});

gulp.task('js', function() {
  return gulp.src('src/js/*.js')
    .pipe(gulp.dest('build/js'))
    .pipe(server.stream());
});

gulp.task("serve", ["style"], function() {
  server.init({
    server: "build",
    notify: false,
    open: true,
    ui: false
  });

  gulp.watch("src/sass/**/*.{scss,sass}", ["style"]);
  gulp.watch("src/*.html", ["copy_html"]);
  gulp.watch("build/*.html").on("change", server.reload);
  gulp.watch('src/js/*.js', ['js']);
});

gulp.task("images", function() {
  return gulp.src("src/img/**/*.{png,jpg,gif}")
    .pipe(imagemin([
      imagemin.optipng({optimizationLevel: 3}),
      imagemin.jpegtran({progressive: true})
    ]))
    .pipe(gulp.dest("src/img"));
});

gulp.task("symbols", function() {
  return gulp.src("img/icon/*.svg")
    .pipe(svgmin())
    .pipe(svgstore({
      inlineSvg: true
    }))
    .pipe(rename("symbols.svg"))
    .pipe(gulp.dest("img"));
});

gulp.task("svgmin", function() {
  return gulp.src("src/img/**/*.svg")
    .pipe(svgmin())
    .pipe(gulp.dest("src/img"));
})

gulp.task("copy", function() {
  return gulp.src([
    "src/fonts/**/*.*",
    "src/*.html",
    "src/js/*.js",
    "src/img/*.*",
    "src/icon/*.*",
    "src/manifest.json"
  ], {
    base: "src"
  })
    .pipe(gulp.dest("build"));
});

gulp.task("copy_html", function() {
  return gulp.src("src/*.html", {base: "src"})
    .pipe(gulp.dest("build"));
});

gulp.task("build", function(fn) {
  run("images", "svgmin", "copy", "style", fn);
});
