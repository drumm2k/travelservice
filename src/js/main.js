'use strict';

window.onload = function() {
  requestAnimationFrame(parallaxAnim);
}

// Parallax
const ticket = document.querySelector('.page-header__img');
const pass = document.querySelector('.page-header__img--passport');

function parallaxAnim() {
  let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

  ticket.style.transform = 'translateY(' + scrollTop.toFixed(0) * 0.5 + 'px)';
  pass.style.transform = 'translateY(' + scrollTop.toFixed(0) * 0.5 + 'px)';

  requestAnimationFrame(parallaxAnim);
}

// Smooth scrolling to anchor links (not suppported in IE & Safari)
const links = document.querySelectorAll('.main-nav__list a, .alt-nav__list a');

Array.from(links).forEach(links => {
  const href = links.getAttribute('href');
  const section = document.querySelector(href);
  const offset = 30;

  links.onclick = e => {
    const bodyRect = document.body.getBoundingClientRect().top;
    const sectionRect = section.getBoundingClientRect().top;
    const sectionPosition = sectionRect - bodyRect;
    const offsetPosition = sectionPosition - offset;

    e.preventDefault();

    window.scrollTo({
      top: offsetPosition,
      behavior: 'smooth'
    })
  }
})

// Map
function initMap() {
  var tsMarker = {lat: 59.886392, lng: 30.325176};
  var contentString = 'ул. Коли Томчака д. 28, лит. А, офис 316';

  if (window.innerWidth < 768) {
    var mapCenter = {lat: 59.886392, lng: 30.326176};
  }
  else {
    var mapCenter = {lat: 59.886392, lng: 30.307176};
  }

  var map = new google.maps.Map(document.getElementById('map'), {
    center: mapCenter,
    zoom: 14,
    fullscreenControl: false,
    mapTypeControl: false,
    scrollwheel:  false,
    streetViewControl: false,
    styles: [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "#8DAABD"
          }
        ]
      },
      {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      },
      {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#bfbebe"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dadada"
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      },
      {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      },
      {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#8DAABD"
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }
    ]
  });

  var marker = new google.maps.Marker({
    map: map,
    position: tsMarker,
    icon: '/img/pin.svg',
    title: 'Travel Service'
  });

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}
